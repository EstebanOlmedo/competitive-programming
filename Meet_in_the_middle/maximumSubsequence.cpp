/*
 * Judge:  Kattis
 * Topic: Meet in the middle, binary search
 * Link: https://codeforces.com/problemset/problem/888/E
 */
#include<bits/stdc++.h>
#define lli long long int
#define forn(i,n) for(int i = 0; i < n; i++)
#define fastIO(); ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define pb push_back
#define fi first
#define se second
using namespace std;
lli num[40], m;
int n, fiPar, sePar;
map<lli, int> ma;
void build(){
	for(int i = 0; i < (1<<sePar); ++i){
		lli sumAct = 0;
		for(int j = 0; j < sePar; j++){
			if(i & (1<<j)){
				sumAct = (sumAct + num[j+fiPar])%m;
			}
		}
		ma[sumAct] = i;
	}
}

void solve(){
	cin >> n >> m;
	forn(i,n) cin >> num[i];
	fiPar = n/2;
	sePar = n-fiPar;
	build();
	pair<int, int> ans;
	lli resOp = 0;
	if(n == 1) { cout << num[0]%m << '\n'; return;}
	for(int i = 0; i < (1<<fiPar); i++){
		lli sumAct = 0;
		forn(j, fiPar){
			if(i & (1<<j)){
				sumAct = (sumAct + num[j])%m;
			}
		}
		lli falta = m-1-sumAct;
		// cout << "sum: " << sumAct << " falta: " << falta << '\n';
		auto it = ma.upper_bound(falta);
		auto it2 = ma.lower_bound(falta);
		if(it2 != ma.end()){
			//cout << "it2: "<< (*it2).fi << '\n';
			if((*it2).fi == falta) resOp = m-1;
			else if(((*it2).fi +sumAct)%m > resOp){
				resOp = ((*it2).fi +sumAct)%m;
			}
		}/* 
		else{
			it2--;
			//cout << "it2: "<< (*it2).fi << '\n';
			if((*it2).fi == falta) resOp = m-1;
			else if(((*it2).fi +sumAct)%m > resOp){
				resOp = ((*it2).fi +sumAct)%m;
			}
		}*/
		if(it != ma.begin()) it--;
		if((*it).fi == falta){
			resOp= m-1;
		}
		else{
			if(((*it).fi + sumAct)%m > resOp){
				resOp = ((*it).fi + sumAct)%m;
			}
		}
	}
	cout << resOp << '\n';
}

int main(){
	fastIO();
	solve();
	return 0;
}
