/*
 * Judge: Codeforces
 * Topic: Greedy, implementation
 * Link: https://codeforces.com/problemset/problem/479/C
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; ++i)
#define lli long long int
#define se second
#define fi first
using namespace std;
typedef pair<int,int> pii;
pii num[5005];
int n;
void solve(){
	cin >> n;
	forn(i,n) cin >> num[i].fi >> num[i].se;
	sort(num, num+n);
	lli diaAct = 1;
	forn(i,n){
		if(diaAct > num[i].se){
			diaAct = num[i].fi;
		}
		else{
			diaAct = num[i].se;
		}
	}
	cout << diaAct << '\n';
}
int main(){
	solve();
	return 0;
}
