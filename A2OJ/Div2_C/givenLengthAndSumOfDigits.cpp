/*
 * Judge: Codeforces
 * Topic: DP, greedy, implementation
 * Link: https://codeforces.com/problemset/problem/489/C
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define fi first
#define se second
#define pb push_back
#define lli long long int
using namespace std;
int m, s, aux, sumAct;
vector<int> ans;
int main(){
	cin >> m >> s;
	if(9*m < s || (s ==  0 && m != 1)){
		cout << "-1 -1\n";
	}
	else if(m == 1 && s == 0) cout << "0 0\n";
	else{
		aux = s;
		sumAct = 1;
		vector<int> mini(m,0);
		mini[0] = 1;
		int j = m-1;
		while(sumAct < aux){
			if(mini[j] == 9){
				j--;
			}
			mini[j]++;
			sumAct++;
		}
		for(auto x : mini) cout <<x;
		cout << ' ';
		while(s >= 10) {
			ans.pb(9); 
			s-=9;
		}
		ans.pb(s);
		while(ans.size() < m) ans.pb(0);
		for(int i = 0; i < m; i++){
			cout << ans[i];
		}
		cout << '\n';
	}
	return 0;
}
