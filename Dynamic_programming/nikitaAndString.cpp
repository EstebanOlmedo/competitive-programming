/*
 * Judge: Codeforces
 * Topic: Dynamic programming
 * Link: https://codeforces.com/problemset/problem/877/B
 */
#include<bits/stdc++.h>
using namespace std;
string cad;
int n;
int mem[5003][3];
int res;
int fun(int i, int estado)
{
	if(i == n) return 0;
	if(i == n - 1)
	{
		if((estado == 3 || estado == 1) && cad[i] == 'a') return 1;
		else if(estado == 2 && cad[i] == 'b') return 1;
		else return 0;
	}
	if(mem[i][estado] != -1) return mem[i][estado];
	if(estado == 1)
	{
		if(cad[i] == 'a')
			mem[i][estado] = 1 + max(fun(i+1, 1), fun(i+1,2));
		else
			mem[i][estado] = max(fun(i+1, 1), fun(i+1,2));
	}
	if(estado==2)
	{
		if(cad[i] == 'b')
			mem[i][estado] = 1 + max(fun(i+1,2), fun(i+1,3));
		else
			mem[i][estado] = max(fun(i+1,2), fun(i+1,3));
	}
	if(estado==3)
	{
		if(cad[i] == 'a')
			mem[i][estado] = 1 + fun(i+1,3);
		else
			mem[i][estado] = fun(i+1,3);
	}

	return mem[i][estado];
}
int main(void)
{
	memset(mem, -1, sizeof mem);
	cin >> cad;
	n = cad.size();
	res = max(fun(0,1), fun(0,2));
	cout << res << "\n";

	return 0;
}
