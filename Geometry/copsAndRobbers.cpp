/*
 * Judge: UVa
 * Topic: Geometry, polygons, convex hull
 * Link: https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=297
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define lli long long int
#define ld long double
#define pb push_back
using namespace std;
int c,r,o;
struct point{
	lli x,y;
	point(): x(0), y(0){}
	point(lli _x, lli _y): x(_x), y(_y){}

	lli cross(const point& p)const{return x*p.y-y*p.x;}
	lli dot(const point& p)const{return x*p.x+y*p.y;}
	point operator-(const point& p)const{return point(x-p.x, y-p.y);}
	bool operator<(const point& p)const{
		if(x== p.x) return y<p.y;
		return x<p.x;
	}
	bool operator==(const point& p)const{
		return x== p.x && y == p.y;
	}
};
double orient(point a, point b, point c){
	return (b-a).cross(c-a);
}
bool inDisk(point a, point b, point p){
	return (a-p).dot(b-p) <= 0;
}
bool onSegment(point a, point b, point p){
	return (orient(a,b,p) == 0 && inDisk(a,b,p)) || (p==b || p==a);
}
bool above(point a, point p){
	return p.y >= a.y;
}
bool crossesRay(point a, point p, point q){
	return (above(a,q) - above(a,p)) * orient(a,p,q) > 0;MX

}
bool isInPolygon(vector<point>& p, point a, int tamOr){
	int numCrosses = 0;
	int n = p.size();
	//if(n < 3) return false;
	forn(i,n){
		if(onSegment(p[i], p[(i+1)%n],a)){
			return tamOr >= 3? true: false;
		}
		numCrosses += crossesRay(a,p[i], p[(i+1)%n]);
	}
	return numCrosses & 1;
}
vector<point> convexHull(vector<point> P){
	P.erase(unique(P.begin(), P.end()), P.end());
	int n = P.size();
	if(n <= 2) return P;
	sort(P.begin(), P.end());
	vector<point> U, L;
	forn(i,n){
		while(L.size() >= 2 && (L[L.size()-1]-L[L.size()-2]).cross(P[i]-L[L.size()-1]) <= 0)
			L.pop_back();
		L.pb(P[i]);
	}
	for(int i = n-1; ~i;--i){
		while(U.size() >= 2 && (U[U.size()-1]-U[U.size()-2]).cross(P[i]-U[U.size()-1]) <= 0)
			U.pop_back();
		U.pb(P[i]);
	}
	U.pop_back(); L.pop_back();
	L.insert(L.end(), U.begin(), U.end());
	return L;
}
ostream &operator<<(ostream& os, const point& p){
	return os << "(" << p.x << "," << p.y << ")";
}

int main(){
	int cont = 0;
	while(cin >> c >> r >> o &&(c||r||o)){
		vector<point> cops(c);
		vector<point> robbers(r);
		vector<point> others(o);
		forn(i,c) cin >> cops[i].x >> cops[i].y;
		forn(i,r) cin >> robbers[i].x >> robbers[i].y;
		forn(i,o) cin >> others[i].x >> others[i].y;
		vector<point> conCops = convexHull(cops);
		vector<point> conRobb = convexHull(robbers);
		cout << "Data set " << ++cont <<":\n";
		forn(i,o){
			bool danger = false;
			bool safe = false;
			danger = isInPolygon(conRobb, others[i], r);
			safe = isInPolygon(conCops, others[i], c);
			if(safe){
				cout << "     Citizen at " << others[i] << " is safe.\n";
			}
			else if((danger)){
				cout << "     Citizen at " << others[i] << " is robbed.\n";
			}
			else{
				cout << "     Citizen at " << others[i] << " is neither.\n";
			}
		}
		cout << "\n";
	}
}
