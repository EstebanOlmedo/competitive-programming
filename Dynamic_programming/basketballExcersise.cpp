/*
 * Judge: Codeforces
 * Topic: Dynamic programming
 * Link: https://codeforces.com/problemset/problem/1195/C
 */
#include<bits/stdc++.h>
using namespace std;
long long mem [100004][3];
long long fila1[100004];
long long fila2[100004];
int n;

long long fun(int i, int anterior)
{
	if(i > n) return 0;
	if(i == n && anterior == 0) return max(fila1[n], fila2[n]);
	if(i == n && anterior == 1) return fila2[n];
	if(i == n && anterior == 2) return fila1[n];
	if(mem[i][anterior] != -1) return mem[i][anterior];
	if(anterior==0)
		mem[i][anterior] = max(max(fun(i+1,1)+fila1[i], fun(i+1,2)+fila2[i]), fun(i+1, 0));
	if(anterior==1)
		mem[i][anterior] = max(fun(i+1,2)+fila2[i], fun(i+1, 0));
	if(anterior==2)
		mem[i][anterior] = max(fun(i+1,1)+fila1[i], fun(i+1, 0));

	return mem[i][anterior];
}

int main()
{
	memset(mem, -1, sizeof mem);
	cin >> n;
	for(int i = 0; i < n; i++)
		cin >> fila1[i];
	for(int i = 0; i < n; i++)
		cin >> fila2[i];
	cout << fun(0, 0) << "\n";

	return 0;
}
