/*
 * Judge: Codeforces
 * Topic: Binary search
 * Link: https://codeforces.com/problemset/problem/1201/C
 */
#include <bits/stdc++.h>
using namespace std;

long long l, r;
long long MAX = 3000000000;
long long arr [200005];
long long res;
int mediana, n;
long long mitad, k;

bool sePuedeLograr(long long x, int mov)
{
	for(int i = mediana; i < n; i++)
	{
		if(arr[i] >= x)
			return true;
		else if(mov >= x - arr[i])
			mov -= (x - arr[i]);
		else
			return false;
	}

	return true;
}

int main(void)
{
	cin >> n >> k;
	for(int i = 0; i < n; i++)
		cin >> arr[i];
	sort(arr, arr+n);
	mediana = n/2;
	l = arr[mediana];
	r = MAX;
	while(l <= r)
	{
		mitad = (l + r) / 2;
		if(sePuedeLograr(mitad, k))
		{
			res = mitad;
			l = mitad + 1;
		}
		else
			r = mitad - 1;
	}

	cout << res << "\n";

	return 0;
}
