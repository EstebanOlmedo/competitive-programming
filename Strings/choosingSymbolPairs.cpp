/*
 * Judge: Codeforces
 * Topic: Strings
 * Link: https://codeforces.com/problemset/problem/50/B 
 */
#include<bits/stdc++.h>
using namespace std;
long long contar(string& s){
	long long res = 0;
	int tam = s.size();
	int i = 0;
	while(i < tam){
		char act = s[i];
		long long sumar = 0;
		while(i < tam && act == s[i]){
			sumar++;
			i++;
		}
		res += (sumar*sumar);
	}
	return res;
}
int main(){
	string s;
	cin >> s;
	sort(s.begin(), s.end());
	cout << contar(s) << "\n";
	return 0;
}
