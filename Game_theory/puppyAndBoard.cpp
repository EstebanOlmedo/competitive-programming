/*
 * Judge: Codechef
 * Topic: Game theory
 * Link: https://www.codechef.com/problems/TUZGMBR
 */
#include<bits/stdc++.h>
using namespace std;
int n, m, t;
void solve(){
        cin >> n >> m;
        if((n-1)%4 ^ (m-1)%3) cout << "Tuzik\n";
        else cout << "Vanya\n";
}
int main(){
        cin >> t;
        while(t--)
                solve();
        return 0;
}
