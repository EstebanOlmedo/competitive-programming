/*
 * Judge: Spoj
 * Topic: Game theory, grundy numbers
 * Link: https://www.spoj.com/problems/ADAFIMBR/en/
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define forr(i,a,n) for(int i = a; i < n; i++)
#define fastIO(); ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
using namespace std;
const int N = 3000005;
int fibo[34];
int G[N];
int n, m, ans, cont;
void precal(){
	fibo[1] = fibo[2] = 1;
	forr(i,3,33) fibo[i] = fibo[i-1]+fibo[i-2];
	//cout << fibo[32] << '\n';
	//forr(i,1,34) cout << fibo[i] << ' ';
	//cout << endl;
}
/*
int getMEX(set<int>& pas){
	for(int i = 0; ; i++){
		if(!pas.count(i))
			return i;
	}
}
*/
void getGrundy(){
	forr(i,1,N){
		vector<bool> shrek (20,false);
		forr(k,1,33){
			if(i - fibo[k] >= 0){
				shrek[G[i-fibo[k]]] = true;
				//if(G[i-fibo[k]] >= 20) cout << "Hay pedo\n";
			}
		}
		int k = 0;
		while(shrek[k]){k++;}
		G[i] = k;
	}
	/*
	forn(i,1000) cout << G[i] << ' ';
	cout << endl;
	*/
}
int main(){
	fastIO();
	precal();
	getGrundy();
	//cout << "Precal\n";
	cin >> n;
	forn(i,n){
		cin >> m;
		ans ^= G[m];
	}
	cout << (!ans? "Vinit\n" : "Ada\n");
	return 0;
}
