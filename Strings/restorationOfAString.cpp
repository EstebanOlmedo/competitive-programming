    #include<bits/stdc++.h>
    using namespace std;
    int t;
    bool posible = true;
    int grafo[26];
    bool esHijo[26];
    bool existe[26];
    bool aniadirGrafo(string& s){
    	vector<bool> pasados(26, false);
    	int i = 0;
    	int tam = s.size() - 1;
    	while(i < tam){
    		existe[s[i] - 'a'] = true;
    		if(pasados[s[i] - 'a']) return false;
    		if(grafo[s[i] - 'a'] == -1){
    			grafo[s[i] - 'a'] = s[i + 1] - 'a';
    			pasados[s[i] - 'a'] = true;
    			esHijo[s[i + 1] - 'a'] = true;
    		}
    		else{
    			if(grafo[s[i] - 'a'] != s[i + 1] - 'a') return false;
    			pasados[s[i] - 'a'] = true;
    		}
    		i++;
    	}
    	if(pasados[s[i] - 'a']) return false;
    	existe[s[i] - 'a'] = true;
    	return true;
    }
    bool hayCiclo(int i, vector<bool>& visitados){
    	while(i != -1){
    		if(visitados[i]) return true;
    		visitados[i] = true;
    		i = grafo[i];
    	}
    	return false;
    }
    bool hayCiclo(){
    	int i = 0;
    	bool encontro = false;
    	vector<bool> visitados (26, false);
    	while(i < 26){ 
    		while(i < 26 && (!existe[i] || esHijo[i])) i++;
    		if(i == 26) break;
    		encontro = true;
    		if(hayCiclo(i, visitados)) return true;
    		i++;
    	}
    	for(int i = 0; i < 26; i++){
    		if(existe[i] && !visitados[i]) return true;
    	}
    	if(encontro)
    		return false;
    	else
    		return true;
    }
    void imprimirCadena(){
    	for(int i = 0; i < 26; i++){
    		if(!esHijo[i] && existe[i]){
    			int actual = i;
    			char car = i + 'a';
    			cout << car;
    			while(grafo[actual] != -1){
    				car = grafo[actual] + 'a';
    				actual = grafo[actual];
    				cout << car;
    			}
    		}
    	}
    	cout << "\n";
    }
    int main(){
    	memset(grafo, -1, sizeof(grafo));
    	cin >> t;
    	while(t--){
    		string s;
    		cin >> s;
    		posible = posible && aniadirGrafo(s);
    	}
    	if(posible && !hayCiclo())
    		imprimirCadena();
    	else
    		cout << "NO\n";
    	return 0;
    }
