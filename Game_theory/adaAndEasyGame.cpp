/*
 * Judge: Spoj
 * Topic: Game theory, grundy numbers
 * Link: https://www.spoj.com/problems/ADAGAME2/en/
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define fastIO(); ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
using namespace std;
int t, m;
typedef long long ll;
typedef vector<int> vi;
const int N = 1e5 + 5;
int R[100005];
int pil[100005];
int cont = 0;
//int G[100005];
/*
int getMex(set<int> num){
	for(int i = 0; ; i++){
		if(!num.count(i))
			return i;
	}
	return 0;
}*/
int getMex(priority_queue<int,vi,greater<int> > num){
	int i = 0;
	while(!num.empty() && i == num.top()){
		i = num.top() + 1;
		num.pop();
	}
	return i;
}
vi getGrundy(int n,int m){
	vi G(n+1);
	for(int i = 0; i <= n; i++){
		//priority_queue<int, vi, greater<int> > act;
		vector<bool> shrek (35, false);
		forn(j,m){
			if(i - R[j] >= 0){
				shrek[G[i-R[j]]] = true;
			}
			int k = 0;
			while(shrek[k]) k++;
			G[i] = k;
		}
	}
	return G;
}
int main(){
	fastIO();
	cin >> t;
	while(t--){
		int tam;
		int maxi = -1;
		cin >> tam >> m;
		forn(i,m){
			cin >> R[i];
		}
		vi G = getGrundy(N,m);
		int res = 0;
		forn(i,tam){
			cin >> pil[i];
		}
		forn(i,tam){
			res ^= G[pil[i]];
		}
		cout << (res == 0? "Vinit\n": "Ada\n");
	}
	return 0;
}
