/* 
 * Judge: UVa
 * Topic: Geometry, convex hull, rotating calipers
 * Link: https://onlinejudge.org/index.php?option=onlinejudge&Itemid=8&page=show_problem&problem=3729
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0;i<n;++i)
#define pb push_back
#define lli long long int
#define ld long double
#define inf 1e+30
using namespace std;
const ld eps = 0;
bool ge(ld a, ld b){return a-b > eps;}
bool le(ld a, ld b){return b-a > eps;}
bool leq(ld a, ld b){return b-a >= eps;}
bool eq(ld a, ld b){return abs(b-a) <= eps;}
struct point{
	ld x,y;
	point(): x(0), y(0){}
	point(ld _x, ld _y): x(_x), y(_y) {}

	ld cross(const point& p)const{return x*p.y - y*p.x;}
	ld dot(const point& p)const{return x*p.x+y*p.y;}
	ld length()const{return sqrtl(x*x+y*y);}
	point perp(){return point(-y,x);}
	point operator-(const point& p)const{return point(x-p.x,y-p.y);}
	bool operator<(const point& p){
		if(eq(x,p.x)) return le(y , p.y);
		return le(x ,p.x);
	}
};
vector<point> convexHull(vector<point> P){
	int n = P.size();
	if(n <= 2) return P;
	sort(P.begin(), P.end());
	vector<point> L,U;
	forn(i,n){
		while(L.size() >= 2 &&leq((L[L.size()-1] - L[L.size()-2]).cross(P[i]-L[L.size()-1]),0))
			L.pop_back();
		L.pb(P[i]);
	}
	for(int i = n-1; ~i; --i){
		while(U.size() >= 2 && leq((U[U.size()-1] - U[U.size()-2]).cross(P[i]-U[U.size()-1]),0))
			U.pop_back();
		U.pb(P[i]);
	}
	L.pop_back(); U.pop_back();
	L.insert(L.end(), U.begin(), U.end());
	return L;
}
ld distancePointLine(point a, point v, point b){
	return abs(v.cross(b-a))/v.length();
}

pair<ld,ld> smallestEnclosingRectangle(vector<point>& P){
	ld per = inf;
	ld are = inf;
	int n = P.size();
	for(int i = 0, j = 0, k = 0, l= 0; i < n; ++i){
		while(ge((P[(i+1)%n] - P[i]).dot(P[(j+1)%n] - P[j]) ,0))
			j = (j+1)%n;
		if(i == 0) k = j;
		while(ge((P[(i+1)%n] - P[i]).cross(P[(k+1)%n] - P[k]), 0))
			k = (k+1)%n;
		if(i == 0) l = k;
		while(le((P[(i+1)%n] - P[i]).dot(P[(l+1)%n] - P[l]),0))
			l = (l+1)%n;
		point v = P[(i+1)%n] - P[i];
		ld base = distancePointLine(P[i], v, P[k]);
		ld altura = distancePointLine(P[j],v.perp(),P[l]);
		per = min(per,2*(altura+base));
		are = min(are,altura*base);
	}
	return {are,per};
}

int main(){
	int n;
	while(cin >> n && n){
		vector<point> P(n);
		forn(i,n) cin >> P[i].x >> P[i].y;
		vector<point> convex = convexHull(P);
		pair<ld,ld> val = smallestEnclosingRectangle(convex);
		cout << fixed << setprecision(2) << val.first << ' '<< val.second << '\n';
	}
}
