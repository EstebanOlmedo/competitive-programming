/*
 * Judge:  POJ
 * Topic: Meet in the middle, brute force, binary search
 * Link: http://poj.org/problem?id=2785
 */
#include<bits/stdc++.h>
#define lli long long int
using namespace std;
const int N = 2600;
lli a[N], b[N], c[N], d[N], n;
vector<int> num;
void build(){
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++)
			num.push_back(c[i] + d[j]);
	}
	sort(num.begin(), num.end());
}
void solve(){
	cin >> n;
	lli ans = 0;
	num.reserve(n*n);
	for(int i = 0; i < n; i++){
		cin >> a[i] >> b[i] >> c[i] >> d[i];
	}
	build();
	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			lli sum = a[i] + b[j];
			auto it1 = lower_bound(num.begin(), num.end(), -sum);
			auto it2 = upper_bound(num.begin(), num.end(), -sum);
			int l = it1-num.begin();
			if(it2 != num.begin()){
				it2--;
			}
			int r = it2-num.begin();
			if(it1 == num.end()) continue;
			if(*it1 == -sum && *it2 == -sum){
				ans += r-l+1;
			}
		}
	}
	cout << ans << '\n';
}

int main(){
	ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
	solve();
	return 0;
}
