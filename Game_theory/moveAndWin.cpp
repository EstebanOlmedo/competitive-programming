/*
 * Judge: AtCoder
 * Topic: Game theory
 * Link: https://atcoder.jp/contests/agc020/tasks/agc020_a?lang=en
 */
#include<iostream>
using namespace std;

int main()
{
	int n, a, b;
	cin >> n >> a >> b;
	if(abs(a-b)&1)
		cout << "Borys\n";
	else
		cout << "Alice\n";

	return 0;
}
