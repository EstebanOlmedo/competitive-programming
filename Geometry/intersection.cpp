/*
 * Judge: POJ or VJudge
 * Topic: Geometry, lines
 * Link: https://vjudge.net/problem/POJ-1410 or http://poj.org/problem?id=1410
 */
//#include<bits/stdc++.h>
#include<iostream>
#include<iomanip>
#define forn(i,n) for(int i = 0;  i< n; ++i)
using namespace std;
const double eps = 1e-5;
int n;
bool eq(double a, double b){
	return abs(a-b) <= eps;
}
struct point{
	int x,y;

	point(): x(0.0), y(0.0){}
	point(double x, double y): x(x), y(y) {}

	point operator-(const point& p) const {return point(x-p.x, y-p.y);}
	point operator+(const point& p) const {return point(x+p.x, y+p.y);}
	point operator*(const int val) const {return point(x*val, y*val);}
	point operator/(const int val) const {return point(x/val, y/val);}
	int cross(const point& b){
		return x*b.y - y*b.x;
	}
	int dot(const point& b){
		return x*b.x + y*b.y;
	}
};

istream &operator>>(istream &is, point & p){
	return is >> p.x >> p.y;
}
bool pointInSegment(point a, point b, point p){
	return (b-a).cross(p-a) == 0 && (a-p).dot(b-p) <= 0;
}
int sgn(int x){
	if(x > 0) return 1;
	if(x < 0) return -1;
	return 0;
}
int intersectSegmentsInfo(point& a, point& b, point& c, point& d){
	point v1 = b-a, v2 = d-c;
	int t = sgn(v1.cross(c-a)), u = sgn(v1.cross(d-a));
	if(t == u){
		if(t == 0){
			if(pointInSegment(a,b,c) || pointInSegment(a,b,d) || 
					pointInSegment(c,d,a) || pointInSegment(c,d,b)){
				return -1;
			}
			else{
				return 0;
			}
		}
		else{
			return 0;
		}
	}
	else{
		return sgn(v2.cross(a-c)) != sgn(v2.cross(b-c));
	}
}
bool betweenSeg(point a1, point a, point b, point c, point d){
	if(a1.x > a.x && a1.x < b.x && a1.y > d.y && a1.y < b.y) return true;
	return false;
}
void solve(){
	cin >> n;
	point a, b;
	point rect[4];
	forn(i,n){
		bool pos = false;
		cin >> a >> b >> rect[0] >> rect[2];
		if(rect[0].x > rect[2].x) swap(rect[0],rect[2]);
		rect[1] = point(rect[2].x,rect[0].y);
		rect[3] = point(rect[0].x,rect[2].y);
		if(rect[0].y < rect[3].y) swap(rect[0],rect[3]);
		if(rect[1].y < rect[2].y) swap(rect[1],rect[2]);
		/*forn(i,4){
			cout << rect[i].x << ' ' << rect[i].y << '\n';
		}*/
		forn(i,4){
			if(intersectSegmentsInfo(a,b,rect[i], rect[(i+1)%4]))
				pos = true;
		}
		if(betweenSeg(a,rect[0], rect[1], rect[2],rect[3]))
			pos = true;
		cout << (pos? "T\n": "F\n");
	}
}

int main(){
	solve();
	return 0;
}
