/*
 * Judge: OmegaUp
 * Topic: Binary search
 * Link: https://omegaup.com/arena/problem/Planetas#problems 
 */
#include<iostream>
#include<iomanip>
#include<bits/stdc++.h>
using namespace std;
int planetas[1003];
int n;
int k;
double fun(double pos, int ind)
{
	double fuerza = 0.0;
	for(int i = 0; i  < n; i++)
	{
		fuerza += (1.0 / ((double) planetas[i] - pos));
	}
	return fuerza;
}
int main()
{
	cin >> n;
	for(int i = 0; i < n; i++)
		cin >> planetas[i];
	sort(planetas, planetas + n);
	cout << n - 1 << "\n";
	for(int i = 0; i < n - 1; i++)
	{
		double l = (double)planetas[i];
		double r = (double)planetas[i + 1];
		double mitad;
		for(int j = 0; j < 30; j++)
		{
			mitad = l + (r - l)/2.0;
			double f = fun(mitad, i);
			if(f < 0.000)
				l = mitad;
			else if(f > 0.000)
				r = mitad;
			else break;
		}
		cout << fixed << setprecision(3) << mitad << " ";
	}
	cout << "\n";
	return 0;
}
