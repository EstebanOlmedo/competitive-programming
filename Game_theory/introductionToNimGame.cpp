/*
 * Judge: HackerRank
 * Topic: Game theory
 * Link: https://www.hackerrank.com/challenges/nim-game-1/problem
 */
#include<bits/stdc++.h>
using namespace std;
int t;
int num;
int n;
int res;
int main(){
	cin >> t;
	while(t--){
		res = 0;
		cin >> n;
		for(int i = 0; i < n; i++){
			cin >> num;
			res = res xor num;
		}
		if(res) cout << "First\n";
		else cout << "Second\n";
	}
	return 0;
}
