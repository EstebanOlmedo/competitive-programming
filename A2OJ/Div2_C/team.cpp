/*
 * Judge: Codeforces
 * Topic: Implementation, greedy
 * Link: https://codeforces.com/problemset/problem/401/C
 */
#include<bits/stdc++.h>
using namespace std;
int n, m;
void solve(){
	cin >> m >> n;
	if(m > n+1 || n > 2*(m+1)) {
		cout << "-1\n";
	}
	else{
		if(m >= n){
			if(m > n)
				cout << "0";
			for(int i = 0; i < n; i++) cout << "10";
			cout << '\n';
		}
		else{
			while(n > m+1 && n){
				cout << "11";
				n-= 2;
				if(m){
					cout << "0";
					m--;
				}
				else break;
			}
			while(n && m){
				if(n){
					cout << "1";	
					n--;
				}
				if(m){
					cout << "0";
					m--;
				}
			}
			if(n>0) cout << "1";
			if(m>0) cout << "0";
			cout << '\n';
		}
	}
}

int main(){
	solve();
	return 0;
}
