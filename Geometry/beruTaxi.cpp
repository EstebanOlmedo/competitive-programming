/*
 * Judge: Codeforces
 * Topic: Geometry, brute force
 * Link:https://codeforces.com/problemset/problem/706/A
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define se second
#define fi first
using namespace std;
const double eps = 1e-7;
const int N = 1005;
typedef pair<double,double> pdd;
bool eq(double a, double b){
	return abs(a-b) < eps;
}
bool geq(double a, double b){
	return a - b >= -eps;
}
bool leq(double a, double b){
	return a - b <= eps;
}
bool neq(double a, double b){
	return !eq(a,b);
}
bool le(double a, double b){
	return !geq(a,b);
}
bool ge(double a, double b){
	return !leq(a,b);
}
double a, b, n;
pdd cord[N];
double vel[N];
double getTime(pdd a, pdd b, double vel){
	double dist = (a.fi - b.fi)*(a.fi-b.fi) + (a.se - b.se)*(a.se - b.se);
	dist = sqrt(dist);
	return dist / vel;
}
void solve(){
	double ans = 1e9;
	pdd orig = {a,b};
	double tiempo;
	forn(i,n){
		tiempo = getTime(orig, cord[i], vel[i]);
		if(le(tiempo, ans))
			ans = tiempo;
	}
	cout << fixed << setprecision(20) << ans << '\n';
}
int main(){
	cin >> a >> b;
	cin >> n;
	forn(i,n) cin >> cord[i].fi >> cord[i].se >> vel[i];
	solve();
	return 0;
}
