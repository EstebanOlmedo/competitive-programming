/*
 * Judge: Codeforces
 * Topic: DP, implementation
 * Link: https://codeforces.com/problemset/problem/431/C
 */
#include<bits/stdc++.h>
#define lli long long int
using namespace std;
lli dp[120], n, k, d;
lli dp2[120];
const lli mod = 1000000007;
int main(){
	cin >> n >> k >> d;
	dp[0] = 1;
	dp2[0] = 1;
	for(int i = 1; i <= n; i++){
		for(int j = 1; j <= k; j++){
			if(i < j) continue;
			dp[i] = (dp[i] + dp[i-j])%mod;
		}
	}
	for(int i = 1; i <= n; i++){
		for(int j = 1; j < d; j++){
			if(i < j) continue;
			dp2[i] = (dp2[i] + dp2[i-j])%mod;
		}
	}
	//cout << dp[n] << '\n';
	//cout << dp2[n] << '\n';
	lli ans = (dp[n] - dp2[n])% mod;
	if(ans < 0) ans += mod;
	cout << ans<< '\n';
	return 0;
}
