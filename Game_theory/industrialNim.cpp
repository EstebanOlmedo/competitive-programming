/*
 * Judge: Codeforces
 * Topic: Game theory
 * Link: https://codeforces.com/problemset/problem/15/C
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i=0; i <n; i++)
using namespace std;
const int N = 100005;
typedef long long ll;
int n;
ll x[N];
ll m;
ll ans;
ll obtenerXor(ll val, ll tam){
	int mod = tam%4;
	if(!(val&1)){
		switch(mod){
			case 0: return 0;
			case 1: return val + tam - 1;
			case 2: return 1;
			case 3: return val+tam;
		}
	}
	else{
		switch(mod){
			case 0: return val^(val+tam-3) ^ (val+tam-2) ^ (val+tam-1);
			case 1: return val;
			case 2: return val ^ (val + tam-1);
			case 3: return val ^ (val+tam-2) ^ (val+tam-1);
		}
	}
	return -1;
}
int main(){
	cin >> n;
	forn(i,n){
		cin >> x[i] >> m;
		x[i] = obtenerXor(x[i], m);
	}
	forn(i,n){
		ans ^= x[i];
	}
	cout << (ans == 0? "bolik\n":"tolik\n");
}
