#include<bits/stdc++.h>
using namespace std;
int t;
long long n, k;
int main()
{
	cin >> t;
	while(t--){
		cin >> n >> k;
		if(k % 3 == 0){
			if(((n + 1)%(2*(k/3-1)+k/3+3))%3 == 1) cout << "Bob\n";
			else cout << "Alice\n";
		}
		else{
			if(n % 3 == 0) cout << "Bob\n";
			else cout << "Alice\n";
		}
	}
	return 0;
}
