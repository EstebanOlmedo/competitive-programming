/*
 * Judge: Codeforces
 * Topic: Game theory
 * Link: https://codeforces.com/problemset/problem/768/E
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
using namespace std;
int grundy[100];
void precal(){
	forn(i,70){
		grundy[i] = (-1+sqrt(1+8*i))/2;
	}
}
int main(){
	int n;
	cin >> n;
	precal();
	int num;
	int ans = 0;
	forn(i,n){
		cin >> num;
		ans ^= grundy[num];
	}
	cout << (ans? "NO\n": "YES\n");
	return 0;
}
