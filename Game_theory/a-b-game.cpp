/*
 * Judge: Codechef
 * Topic: Game theory
 * Link: https://www.codechef.com/problems/ABGAME
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
using namespace std;
int t;
string s;
void solve(){
	cin >> s;
	int disA = 0;
	int disB = 0;
	int res = 0;
	int par = 0;
	int l, r;
	l = r = 0;
	while(s[r]=='.') r++;
	l = r++;
	while(r < s.size()){
		while(s[r] == '.'&& r<s.size())r++;
		if(r == s.size()){
			s[l] == 'A'? disA += r-(l+1):disB +=r-l-1;
			break;
		}
		if(s[l] != s[r]&r-l-1>0) {
			res ^= r-l-1;
		}
		else{
			s[l] == 'A'? disA += r-(l+1):disB+= r-l-1;
		}
		l = r+1;
		while(s[l] == '.'&&l<s.size())l++;
		r = l+1;
	}
	if(res){
		if(disA == 0 && disB == 0)
			cout << "A\n";
		else{
			if(disA >= disB) cout << "A\n";
			else cout << "B\n";
		}
	}
	else{
		if(disA == 0 && disB == 0)
			cout << "B\n";
		else{
			if(disB >= disA) cout << "B\n";
			else cout << "A\n";
		}
	}
}
int main(){
	cin >> t;
	while(t--){
		solve();
	}
	return 0;
}
