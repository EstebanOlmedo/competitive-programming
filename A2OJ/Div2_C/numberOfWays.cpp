/*
 * Judge: Codeforces
 * Topic: DP, brute force
 * Link: https://codeforces.com/problemset/problem/466/C
 */
#include<bits/stdc++.h>
#define inf 1000000000
#define forn(i,n) for(int i = 0; i<n; i++)
#define lli long long int
#define forr(i,a,n) for(int i = a; i < n;++i)
#define fastIO(); ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define pb push_back
using namespace std;
lli num[500015];
int n, cant[500015];
lli val;
void build(){
	lli sum = 0;
	for(int i = n-1; ~i; --i){
		sum += num[i];
		if(sum == val/3)
			cant[i] = 1;
	}
	for(int i = n-2; ~i; --i){
		cant[i] += cant[i+1];
	}
}
void solve(){
	cin >> n;
	forn(i,n) {
		cin >> num[i];
		val += num[i];
	}
	if(val % 3 != 0) {
		cout << "0\n";
		return;
	}
	build();
	lli ans = 0;
	lli sum = 0;
	for(int i = 0; i +2 < n; ++i){
		sum += num[i];
		if(sum == val/3){
			ans += cant[i+2];
		}
	}
	cout << ans << '\n';
}

int main(){
	fastIO();
	solve();
	return 0;
}
