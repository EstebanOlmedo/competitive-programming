/*
 * Judge: Codeforces
 * Topic: Implementation, greedy
 * Link: https://codeforces.com/problemset/problem/268/C
 */
#include<bits/stdc++.h>
using namespace std;

int n,m, x,y;
int main(){
	cin >> n >> m;
	y = m;
	cout << min(n,m)+1 << '\n';
	while(y >= 0  && x <= n){
		cout << x++ << ' ' << y-- << '\n';
	}
	return 0;
}
