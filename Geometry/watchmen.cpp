/*
 * Judge: Codeforces
 * Topic: Geometry, math, greedy
 * Link: https://codeforces.com/problemset/problem/650/A
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define fi first
#define se second
#define lli long long int
using namespace std;
struct point{
	lli x,y;
	point(): x(0),y(0){}
	point(lli a, lli b): x(a), y(a) {}
	bool operator<(const point b){
		if(x == b.x) return y<b.y;
		return x < b.x;
	}
	bool operator==(const point b){
		return x == b.x && y == b.y;
	}
};
bool compX(const point& A, const point& B){
	if(A.x == B.x) return A.y<B.y;
	return A.x < B.x;
}
bool compY(const point& A, const point& B){
	if(A.y == B.y) return A.x<B.x;
	return A.y < B.y;
}
const int N = 200005;
int n;
lli res;
point puntos[N];
int main(){
	cin >> n;
	forn(i,n){
		cin >> puntos[i].x >> puntos[i].y;
	}
	sort(puntos, puntos+n, compX);
	lli act = puntos[0].x;
	lli sum = -1;
	forn(i,n){
		if(puntos[i].x == act) sum++;
		else{
			act = puntos[i].x;
			res += (sum*(sum+1))/2;
			sum = 0;
		}
	}
	res += (sum*(sum+1)) / 2;
	sort(puntos,puntos+n,compY);
	sum = -1;
	act = puntos[0].y;
	lli iguales = 0;
	forn(i,n){
		if(puntos[i].y == act) {
			sum++;
			if(i > 0 && puntos[i] == puntos[i-1])
				iguales++;
			else{
				res -= (iguales*(iguales+1)/2);
				iguales = 0;
			}
		}
		else{
			res -= (iguales*(iguales+1)/2);
			iguales = 0;
			act = puntos[i].y;
			res += (sum*(sum+1))/2;
			sum = 0;
		}
	}
	if(iguales){
		res -= (iguales*(iguales+1)/2);
	}
	res += (sum*(sum+1)) / 2;
	cout << res << '\n';
}
