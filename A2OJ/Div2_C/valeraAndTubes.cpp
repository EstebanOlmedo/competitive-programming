/*
 * Judge: Codeforces
 * Topic: Greedy
 * Link: https://codeforces.com/problemset/problem/441/C
 */
#include<bits/stdc++.h>
#define pb push_back
#define fi first
#define se second
using namespace std;
typedef pair<int,int> pii;
int n, m, k;
vector<pii> cord[90005];
int main(){
	cin >> n >> m >> k;
	int act = 1;
	for(int i = 1; i <= m; i++){
		for(int j = 1; j <= n; j++){
			if(act == k){
				cord[act].pb({j,i});
			}
			else{
				if(cord[act].size() >= 2) act++;
				cord[act].pb({j,i});
			}
		}
		i++;
		if(i > m) break;
		for(int j = n; j >= 1; j--){
			if(act == k){
				cord[act].pb({j,i});
			}
			else{
				if(cord[act].size() >= 2) act++;
				cord[act].pb({j,i});
			}
		}
	}
	for(int i = 1; i <= k; i++){
		cout << cord[i].size();
		for(auto x :  cord[i]){
			cout << ' ' <<  x.fi << ' ' << x.se;
		}
		cout << '\n';
	}
	return 0;
}
