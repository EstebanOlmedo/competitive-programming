/*
 * Judge: Codeforces
 * Topic: Greedy
 * Link: https://codeforces.com/problemset/problem/478/C
 */
#include<bits/stdc++.h>
using namespace std;
long long int r, b, g, an, ans;
int main(){
	cin >> r >> b >> g;
	if(max({r,b,g}) >2*((b+g+r)-max({r,g,b}))){
		cout << r+g+b-max({r,g,b}) << '\n';
	}else{
		cout << (r+g+b)/3 << '\n';
	}
}
