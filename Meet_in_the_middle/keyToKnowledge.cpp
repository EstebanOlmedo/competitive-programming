/*
 * Judge:  Kattis
 * Topic: Meet in the middle, brute force, Trie
 * Link: https://open.kattis.com/problems/keytoknowledge
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define fi first
#define se second
#define lli long long int
using namespace std;
const int N = 500000;
int t, n, mid, sePar, m;
pair<string, int> s[30];

struct nodo{
	int cont = 0;
	int hijos[30];
	int mask = 0;
};
nodo Trie[N];
int tam = 1;
void push(vector<int>& cad, int maskAct){
	int act = 0;
	for(int x : cad){
		if(Trie[act].hijos[x] != 0)
			act = Trie[act].hijos[x];
		else{
			Trie[act].hijos[x] = tam++;
			act = Trie[act].hijos[x];
		}
	}
	Trie[act].cont++;
	Trie[act].mask = maskAct;
}
int search(vector<int> cad, int* mask){
	int act = 0;
	bool possible = 1;
	for(int x : cad){
		if(Trie[act].hijos[x] != 0)
			act = Trie[act].hijos[x];
		else{
			*mask = 0;
			return 0;
		}
	}
	*mask = Trie[act].mask;
	return Trie[act].cont;
}
void build(){
	forn(i,N){
		memset(Trie[i].hijos, 0, sizeof(Trie[i].hijos));
		Trie[i].cont = 0;
		Trie[i].mask = 0;
	}
	tam = 1;
	forn(i,1<<mid){
		vector<int> isCorrect;
		isCorrect.reserve(n);
		pair<string, int> x;
		forn(k, n){
			x = s[k];
			int act = 0;
			forn(j,mid){
				if(x.fi[j] == '1' && i&(1<<j)){
					act++;
				} 
				else if(x.fi[j] == '0' && !(i&(1<<j))){
					act++;
				}
			}
			if(act <= x.se){
				isCorrect.push_back(x.se - act);
			}else
				break;
		}
		if(isCorrect.size() == n){
			//for(int x : isCorrect) cout << x << ' '; cout << '\n';
			push(isCorrect, i);
		}
	}
}

void solve(){
	lli ans = 0;
	cin >> n >> m;
	mid = m/2;
	sePar = m - mid;
	forn(i,n){
		cin >> s[i].fi >> s[i].se;
	}
	//cout << "Entra:\n";
	build();
	//cout << "Sale\n";
	pair<int, int> maskAns;
	bool usd = 0;
	pair<string, int> x;
	forn(i, (1 << sePar)){
		vector<int> isCorrect;
		isCorrect.reserve(n);
		forn(k,n){
			x = s[k];
			int act = 0;
			forn(j, sePar){
				if(x.fi[j + mid] == '1' && i&(1<<j))
					act++;
				else if(x.fi[j + mid] == '0' && !(i&(1<<j))){
					act++;
				}
			}
			if(act <= x.se){
				isCorrect.push_back(act);
			}
			else break;
		}
		if(isCorrect.size() == n){
			//for(int x : isCorrect) cout << x << ' '; cout << '\n';
			int mask;
			ans += search(isCorrect, &mask);
			if(ans == 1 && !usd){
				maskAns.fi = mask;
				maskAns.se = i;
				usd = 1;
			}
		}
	}
	//vector<int> prueb = {3,2,1};
	//int lsl = 0;
	//cout << "OK: " << search(prueb, &lsl) << '\n';
	if(ans == 1){
		string res;
		forn(i,mid){
			if(maskAns.fi & (1<<i))
				res += '1';
			else
				res += '0';
		}
		forn(i,sePar){
			if(maskAns.se & (1<<i))
				res += '1';
			else
				res += '0';
		}
		cout << res << '\n';
	}
	else{
		cout << ans << ' ' << "solutions\n";
	}
}

int main(){
	ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0);
	cin >> t;
	while(t--){
		solve();
	}
	return 0;
}

