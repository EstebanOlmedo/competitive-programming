/*
 * Judge: Codeforces
 * Topic: Strings, implementation
 * Link: https://codeforces.com/problemset/problem/118/A
 */
#include<bits/stdc++.h>
using namespace std;
string t;
string ans;
set<char> vowels;
void precal(){
	vowels.insert('a');
	vowels.insert('e');
	vowels.insert('i');
	vowels.insert('o');
	vowels.insert('u');
	vowels.insert('y');
}
int main(){
	precal();
	cin >> t;
	for(int i = 0; i < t.size(); i++){
		if(vowels.find(tolower(t[i])) == vowels.end()){
			ans += ".";
			ans += tolower(t[i]);
		}
	}
	cout << ans << '\n';
	return 0;
}
