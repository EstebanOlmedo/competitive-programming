/*
 * Judge: Codeforces
 * Topic: Greedy, implementation
 * Link: https://codeforces.com/problemset/problem/363/C
 */
#include<bits/stdc++.h>
using namespace std;

void solve(){
	string s;
	cin >> s;
	int rep, repAnt;
	bool band = 0;
	char lastLet = 0;
	vector<char> ans;
	for(int i = 0; i < s.size(); i++){
		if(band){
			if(s[i] == lastLet){
			       	rep++;
				if(rep >= 2) {rep--; continue;}
				else {ans.push_back(s[i]);}
			}
			else{
				rep = 1;
				band = 0;
				lastLet = s[i];
				ans.push_back(s[i]);
			}
		}else{
			if(s[i] == lastLet) {
				lastLet = s[i];
				rep++;
				if(rep >= 3) {
					rep--;
					continue;
				}
				else ans.push_back(s[i]);
			}
			else{
				if(rep >= 2) band = 1;
				else band = 0;
				rep = 1;
				lastLet = s[i];
				ans.push_back(s[i]);
			}
		}
	}
	for(char x : ans) cout << x; cout << '\n';
}

int main(){
	ios_base::sync_with_stdio(0); cout.tie(0); cin.tie(0);
	solve();
	return 0;
}
