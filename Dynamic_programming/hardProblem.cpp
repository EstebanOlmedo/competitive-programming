/*
 * Judge: Codeforces
 * Topic: Dynamic programming
 * Link: https://codeforces.com/contest/706/problem/C
 */
#include<bits/stdc++.h>
using namespace std;
long long mem[100006][2];
string cad[100006][2];
long long costo[100006];
long long n, res;
long long inf = 9000000000000000000ll;
string aux;
long long fun(int ind, int estado, int ant)
{
	if(ind >= n)
		return 0;
	if(ind > 0 && cad[ind][estado].compare(cad[ind - 1][ant]) < 0)
		return inf;
	if(mem[ind][estado] != -1) return mem[ind][estado];
	return mem[ind][estado] = min(fun(ind+1, 0, estado), fun(ind+1, 1, estado) + costo[ind + 1]);
}

int main()
{
	memset(mem, -1, sizeof mem);
	cin >> n;
	for(int i = 0; i < n; i++)
		cin >> costo[i];
	for(int i = 0; i < n; i++)
	{
		cin >> aux;
		cad[i][0] = aux;
		reverse(aux.begin(), aux.end());
		cad[i][1] = aux;
	}
	res = min(fun(0, 0, 0), fun(0, 1, 1) + costo[0]);
	if(res == inf)
		cout << "-1" << endl;
	else
		cout << res << endl;

	return 0;
}
