/*
 * Judge: Codeforces
 * Topic: Dynamic programming
 * Link: https://codeforces.com/problemset/problem/166/E
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define lli long long int
#define mod 1000000007
using namespace std;
const int N  = 10000005;
int dp[N][4];
int n;
int main(){
	cin >> n;
	dp[1][0] = 1;
	dp[1][1] = 0;
	dp[1][2] = 0;
	dp[1][3] = 0;
	for(int i = 2; i<=n; i++){
		dp[i][0] = (((dp[i-1][1]+dp[i-1][2]) % mod) +dp[i-1][3])%mod;
		dp[i][1] = (((dp[i-1][2]+dp[i-1][3]) % mod) +dp[i-1][0])%mod;
		dp[i][2] = (((dp[i-1][3]+dp[i-1][0]) % mod) +dp[i-1][1])%mod;
		dp[i][3] = (((dp[i-1][0]+dp[i-1][1]) % mod) +dp[i-1][2])%mod;
	}
	cout << (((dp[n][1]) + dp[n][2] ) % mod + dp[n][3])  % mod << '\n';
}
