/*
 * Judge: Codeforces
 * Topic: Geometry, math
 * Link: https://codeforces.com/problemset/problem/667/A
 */
#include<bits/stdc++.h>
using namespace std;
const double eps = 1e-5;
double d,h,v,e;
double vol;
bool ge(double a, double b){
	return a-b > -eps;
}
double getTime(double vel){
	return vol/vel;
}
bool neq(double a,double b){
	return abs(a-b) >= eps;
}
int main(){
	cin >> d >> h >> v >> e;
	vol = acos(-1)*((d*d)/(4.0))*h;
	e *= (acos(-1)/4.0)*d*d;
	if(ge(v - e, 0.0) && neq(v-e,0.0)){
		cout << "YES\n";
		cout << fixed << setprecision(12) << getTime(v-e) << '\n';
	}
	else
		cout << "NO\n";
	return 0;
}
