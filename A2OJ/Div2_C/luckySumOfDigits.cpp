/*
 * Judge: Codeforces
 * Topic: Math, implementation, brute force
 * Link: https://codeforces.com/problemset/problem/109/A
 */
#include<bits/stdc++.h>
using namespace std;

int main(){
	int n, cp;
	cin >> n;
	cp = n;
	int seven = 0, four = 0;
	if(n % 4 == 0){
		int sobr = n % (7*4);
		seven += n/(28) * 4;
		four += sobr/4;
		for(int i = 0; i< four; i++)
			cout << "4";
		for(int i = 0; i < seven; i++){
			cout << "7";
		}
		cout << '\n';
		return 0;
	}
	while(1){
		n -= 7;
		if(n < 0){
			cout << "-1\n";
			return 0;
		}
		seven++;
		if(n % 4 == 0) break;
	}
	int sobr = n % (7*4);
	seven += n/(28) * 4;
	four += sobr/4;
	for(int i = 0; i< four; i++)
		cout << "4";
	for(int i = 0; i < seven; i++){
		cout << "7";
	}
	cout << '\n';
	return 0;
}
