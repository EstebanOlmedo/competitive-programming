/*
 * Judge: Codeforces
 * Topic: Game theory
 * Link: https://codeforces.com/gym/102452/problem/B
 */
#include<bits/stdc++.h>
using namespace std;
int t, n;
int aux1, aux;

int main(){
	cin >> t;
	while(t--){
		cin >> n;
		for(int i = 0; i < n-1; i++) cin >> aux >> aux1;
		cout << (n&1?"Alice\n": "Bob\n");
	}
	return 0;
}
