/*
 * Judge: POJ
 * Topic: Geometry, lines
 * Link: http://poj.org/problem?id=1269 or https://vjudge.net/problem/POJ-1269
 */
#include<iostream>
#include<iomanip>
#define forn(i,n) for(int i = 0;  i< n; ++i)
using namespace std;
const double eps = 1e-5;
int n;
bool eq(double a, double b){
	return abs(a-b) <= eps;
}
struct point{
	double x,y;

	point(): x(0.0), y(0.0){}
	point(double x, double y): x(x), y(y) {}

	/*istream &operator>>(istream& is){
		return is >> x >> y;
	}*/
	point operator-(const point& p) const {return point(x-p.x, y-p.y);}
	point operator+(const point& p) const {return point(x+p.x, y+p.y);}
	point operator*(const double val) const {return point(x*val, y*val);}
	point operator/(const double val) const {return point(x/val, y/val);}
	double cross(const point& b){
		return x*b.y - y*b.x;
	}
};

	istream &operator>>(istream &is, point & p){
		return is >> p.x >> p.y;
	}
int intersectLinesInfo(point& a1, point& v1, point& a2, point& v2){
	double det = v1.cross(v2);
	if(eq(det,0.0)){
		if(eq((a2-a1).cross(v1),0.0)){
			return -1;
		}
		else{
			return 0;
		}
	}
	else{
		return 1;
	}
}
point intesectLines(point& a1, point& v1, point& a2, point& v2){
	return a1+v1*((a2-a1).cross(v2)/v1.cross(v2));
}
void solve(){
	point a1, a2, b1, b2;
	forn(i,n){
		cin >> a1 >> a2 >> b1 >> b2;
		point v1 = a2-a1;
		point v2 = b2-b1;
		int det = intersectLinesInfo(a1,v1,b1,v2);
		if(det == -1){
			cout << "LINE\n";
		}
		else if(det == 1){
			point inter = intesectLines(a1,v1,b1,v2);
			cout << "POINT ";
			cout << fixed << setprecision(2) << inter.x << ' ' << inter.y << '\n';
		}
		else{
			cout << "NONE\n";
		}
	}
}

int main(){
	cin >> n;
	cout << "INTERSECTING LINES OUTPUT\n";
	solve();
	cout << "END OF OUTPUT\n";
	return 0;
}
