/*
 * Judge: Codeforces
 * Topic: Math, combinatorics
 * Link: https://codeforces.com/problemset/problem/152/C
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define lli long long int
using namespace std;
int n, m;
string s[105];
lli ans;
int main(){
	ios_base::sync_with_stdio(0); cin.tie(0);
	cin >> n >> m;
	ans = 1;
	forn(i,n) cin >> s[i];
	forn(j,m){
		set<char> difLett;
		forn(i,n)
			difLett.insert(s[i][j]);
		ans = ((difLett.size())*ans) % 1000000007;
	}
	cout << ans << '\n';
	return 0;
}
