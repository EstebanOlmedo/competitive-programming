/*
 * Judge: Codeforces
 * Topic: Math, sorting
 * Link: https://codeforces.com/problemset/problem/382/C
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define forr(i,a,n) for(int i = a; i < n; i++)
#define se second
#define fi first
using namespace std;
int num[100005], n;
void isPos(int cse, set<int>& ans){
	int i = 1;
	while(num[i] - num[i-1] == cse) i++;
	int may = num[i-1] + cse;
	if(num[i] - may == cse) {
		ans.insert(may);
	}
}

int main(){
	cin >> n;
	forn(i,n) cin >> num[i];
	if(n == 1)
		cout << "-1\n";
	else if(n == 2){
		set<int> ans;
		if(num[0] > num[1]) swap(num[0], num[1]);
		ans.insert(num[1] - num[0] + num[1]);
		ans.insert(num[0] - (num[1] - num[0]));
		if((num[1] - num[0])&1^1 && num[1] - num[0]){
			ans.insert(num[0] + ((num[1]-num[0])>>1));
		}
		cout << ans.size() << '\n';
		if(ans.size()){
			//sort(ans.begin(), ans.end());
			for(int x : ans) cout << x << ' '; cout << '\n';
		}
	}
	else{
		set<int> ans;
		map<int, int> cont;
		sort(num, num+n);
		forr(i,1,n){
			cont[num[i] - num[i-1]]++;
		}
		for(auto p : cont){
			if(p.se == n-1){
				ans.insert(num[n-1] + p.fi);
				ans.insert(num[0] - p.fi);
			}else if(p.se == n-2){
				isPos(p.fi, ans);
			}
		}
		cout << ans.size() << '\n';
		if(ans.size()) {
			//sort(ans.begin(), ans.end());
			for(int x : ans) cout << x << ' ';
		       	cout << '\n';
		}
	}
}
