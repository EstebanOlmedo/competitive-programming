/*
 * Judge: Codeforces
 * Topic: Geometry, maths
 * Link: https://codeforces.com/problemset/problem/794/B
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define ld long double
using namespace std;
const long double eps = 10e-7;
ld n, h, hAnt, h1, b;
ld A, a;
int main(){
	cin >> n >> h;
	hAnt = 0.0;
	int act = 1;
	A = h / 2.0;
	b = 1.0;
	while(act < n){
		h1 = sqrt(h*h - ((2.0*h*(n-act)*A) / (n*b)));
		//cout << "H1: " << h1 << '\n';
		//cout<< fixed << setprecision(12) << h1+hAnt << '\n';
		cout<< fixed << setprecision(12) << h1 << '\n';
		hAnt += h1;
		//b = ((h-h1)*b)/(2.0*h);
		//h -= h1;
		act++;
	}
	return 0;
}
