/*
 * Judge: Codeforces
 * Topic: Game theory, math
 * Link: https://codeforces.com/problemset/problem/959/A
 */
#include <iostream>
using namespace std;
int main()
{
	long long a;
	cin >> a;
	if(~a&1)
		cout << "Mahmoud\n";
	else
		cout << "Ehab\n";
	return 0;
}
