/*
 * Judge: Codeforces
 * Topic: Dynamic programming
 * Link: https://codeforces.com/problemset/problem/933/A
 */
#include<bits/stdc++.h>
using namespace std;
int res;
int n;
int mem[2002][4];
int cad[2002];
int funBruta(int ind, int estado)
{
		int tomar1 = 0;
		int tomar2 = 0;
		int no_tomar1 = 0;
		int no_tomar2 = 0;
	if(ind >= n) return 0;
	if(estado == 0)
	{
		if(cad[ind] == 1)
		{
			tomar1 = funBruta(ind + 1, 0);
			no_tomar1 = funBruta(ind + 1, 2);
			return max(tomar1, no_tomar1) + 1;
		}
		else
		{
			tomar2 = funBruta(ind + 1, 1);
			no_tomar2 = funBruta(ind + 1, 3);
			return max(tomar2, no_tomar2) + 1;
		}
	}
	else if(estado == 1)
	{
		if(cad[ind] == 1)
		{
			return funBruta(ind +1, 2) + 1;
		}
		else
		{
			tomar2 = funBruta(ind + 1, 1);
			no_tomar2 = funBruta(ind + 1, 3);
			return max(tomar2, no_tomar2) + 1;
		}
	}
	else if(estado == 2)
	{
		if(cad[ind] == 2) return funBruta(ind + 1, 3) + 1;
		else return funBruta(ind + 1, 2) + 1;
	}
	else
	{
		if(cad[ind] == 2) return funBruta(ind + 1, 3) + 1;
		else return 0;
	}
}
int fun(int ind, int estado)
{
	int a = 0;
	int b = 0;
	if(ind >= n)
		return 0;
	if(mem[ind][estado] != 0) return mem[ind][estado];
	switch(estado)
	{
		case 0:
			if(cad[ind] == 1)
				return mem[ind][estado] = fun(ind + 1, 0) + 1;

			else if(cad[ind] == 2)
			{
				int si = fun(ind + 1, 1) + 1; 
				int no = fun(ind + 1, 0);
				return mem[ind][estado] = max(si, no);
			}
		case 1:
			if(cad[ind] == 2)
				return mem[ind][estado] = fun(ind + 1, 1) + 1;
			else 
			{
				a = fun(ind + 1, 1);
				b = fun(ind + 1, 2) + 1;
				return mem[ind][estado] = max(a, b);
			}
		case 2:
			if(cad[ind] == 1) 
				mem[ind][estado] = fun(ind + 1, 2) + 1;
				
			else
			{
				b = fun(ind + 1, 3) + 1;
				a = fun(ind + 1, 2);
				mem[ind][estado] = max(a, b);
			}
			return mem[ind][estado];
		case 3:
			if(cad[ind] == 2) 
				return mem[ind][estado] = fun(ind + 1, 3) + 1;
			else 
				return mem[ind][estado] = fun(ind + 1, 3);
	}
}
int main(void)
{
	int maxi = 0;
	int x = 0;
	cin >> n;
	for(int i = 0; i < n; i++)
		cin >> cad[i];
	/*for(int i = 0; i < n; i++)
	{
			x = fun(i,0);
			if(x > maxi)
				maxi = x;
	}*/
	cout << fun(0,0) << endl;
	return 0;
}
