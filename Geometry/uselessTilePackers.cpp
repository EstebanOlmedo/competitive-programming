/* 
 * Judge: UVa
 * Topic: Geometry, convex hull, area of polygons
 * Link: https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1006
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define pb push_back
#define ld long double
#define lli long long int
using namespace std;
int n;
struct point{
	int x,y;

	point(): x(0), y(0){}
	point(int x, int y): x(x), y(y) {}
	point operator-(const point p) const{return point(x-p.x,y-p.y);}
	bool operator<(const point& p){
		if(x == p.x) return y < p.y;
		return x < p.x;
	}
	int cross(const point p){return x*p.y - y*p.x;}
};
istream &operator>>(istream& s, point& p){return s >> p.x >> p.y;}
ostream &operator<<(ostream &os, const point & p){
	return os << "(" << p.x << ", " << p.y << ")";
}
vector<point> getConvexHull(vector<point>& P){
	vector<point> L, U;
	int m = P.size();
	sort(P.begin(), P.end());
	forn(i,m){
		while(L.size() >= 2 && (L[L.size()-1] -L[L.size()-2]).cross(P[i] - L[L.size()-1]) <= 0){
			L.pop_back();
		}
		L.pb(P[i]);
	}
	for(int i = m-1; ~i; --i){
		while(U.size() >= 2 && (U[U.size()-1] -U[U.size()-2]).cross(P[i] - U[U.size()-1]) <= 0){
			U.pop_back();
		}
		U.pb(P[i]);
	}
	L.pop_back(); U.pop_back();
	L.insert(L.end(), U.begin(), U.end());
	//forn(i,L.size()) cout << L[i].x << ' ' << L[i].y << '\n';
	return L;
}
ld getArea(vector<point>& puntos){
	ld sum = 0.0;
	int m = puntos.size();
	forn(i,m){
		sum += puntos[i].cross(puntos[(i+1)%m]);
		//cout <<"Cross: " << puntos[i] << ' ' << puntos[(i+1)%m] <<' ' << puntos[i].cross(puntos[(i+1)%m])<<'\n';
	}
	return abs(sum / 2.0);
}
void solve(){
	int cont = 0;
	while(cin >> n && n){
		//if(cont) cout << '\n';
		vector<point> puntos;
		point aux;
		forn(i,n){
			cin >> aux;
			puntos.pb(aux);
		}
		ld area1 = getArea(puntos);
		vector<point> convex = getConvexHull(puntos);
		ld area2 = getArea(convex);
		//cout << "AREA POL: " << area1 << '\n';
		//cout << "CONVEX HULL: " << area2 << '\n';
		ld res = 100.00 - ((area1*100.0)/area2);
		cout << "Tile #" << ++cont << '\n';
		cout << "Wasted Space = "<<fixed << setprecision(2) << res << " %\n";
		cout << '\n';
	}
}
int main(){
	solve();
	return 0;
}
