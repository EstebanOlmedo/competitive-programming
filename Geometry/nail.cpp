/*
 * Judge: UVa 
 * Topic: Geometry, convex hull
 * Link: https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=2037
 */
#include<bits/stdc++.h>
#define ld long double
#define forn(i,n) for(int i = 0; i < n; i++)
#define pb push_back
#define lli long long int
using namespace std;
const ld eps = 1e-9;
int n,t;
lli ini;
bool ge(ld a, ld b){return a-b > -eps;}
bool eq(ld a, ld b){return abs(a-b) < eps;}
bool le(ld a, ld b){return a - b < eps;}
struct point{
	ld x, y;

	point(): x(0),y(0){}
	point(ld x, ld y): x(x), y(y) {}

	point operator-(const point p){return point(x-p.x,y-p.y);}
	bool operator<(const point& p){
		if(eq(x,p.x)) return le(y,p.y);
		return le(x,p.x);
	}
	ld cross(point p){ return x*p.y - y*p.x;}
	ld length(){
		return sqrt(x*x+y*y);
	}
};

istream &operator>>(istream& s, point& p){
	return s >> p.x >> p.y;
}
vector<point> getConvexHull(vector<point> P){
	int m = P.size();
	if(m <= 2) return P;
	sort(P.begin(), P.end());
	vector<point> L,U;
	forn(i,m){
		while(L.size()>= 2 && (L[L.size()-1]-L[L.size()-2]).cross((P[i]-L[L.size()-1])) <= 0){
			L.pop_back();
		}
		L.pb(P[i]);
	}
	for(int i = m-1; ~i;--i){
		while(U.size()>= 2 && (U[U.size()-1]-U[U.size()-2]).cross((P[i]-U[U.size()-1])) <= 0){
			U.pop_back();
		}
		U.pb(P[i]);
	}
	U.pop_back(); L.pop_back();
	L.insert(L.end(),U.begin(),U.end());
	return L;
}
ld perimeter(vector<point> P){
	int m = P.size();
	ld res = 0.0;
	forn(i,m){
		res+=(P[(i+1)%m]-P[i]).length();
	}
	return res;
}
void solve(){
	cin >> ini >> n;
	vector<point> pun(n);
	forn(i,n){
		cin >> pun[i];
	}
	vector<point> convex = getConvexHull(pun);
	ld per = perimeter(convex);
	if(ge(ini,per)){
		per = ini;
	}
	cout << fixed << setprecision(5) << per << '\n';
}
int main(){
	cin >> t;
	while(t--){
		solve();
	}
	return 0;
}
