/* 
 * Judge: UVa
 * Topic: Geometry, convex hull, rotating calipers
 * Link: https://onlinejudge.org/index.php?option=onlinejudge&Itemid=8&page=show_problem&problem=3552 
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; ++i)
#define lli long long int
#define ld long double
#define pb push_back
using namespace std;
const ld eps = 1e-4;
int n;
struct point{
	int x,y;
	point(): x(0), y(0) {}
	point(ld _x,ld _y): x(_x),y(_y){}

	int cross(const point p){return x*p.y-y*p.x;}
	point operator*(const ld& c)const{return point(c*x,c*y);}
	point operator-(const point& p)const{return point(x-p.x,y-p.y);}
	point operator/(const ld& c)const{return point(x/c,y/c);}
	bool operator<(const point& p){
		if(x == p.x) return y < p.y;
		return x < p.x;
	}
	ld length(){
		return sqrt(x*x + y*y);
	}
};
vector<point> convexHull(vector<point> P){
	int m = P.size();
	if(m <= 2) return P;
	sort(P.begin(), P.end());
	vector<point> L,U;
	forn(i,m){
		while(L.size()>= 2 && (L[L.size()-1]-L[L.size()-2]).cross((P[i]-L[L.size()-1])) <= 0){
			L.pop_back();
		}
		L.pb(P[i]);
	}
	for(int i = m-1; ~i;--i){
		while(U.size()>= 2 && (U[U.size()-1]-U[U.size()-2]).cross((P[i]-U[U.size()-1])) <= 0){
			U.pop_back();
		}
		U.pb(P[i]);
	}
	U.pop_back(); L.pop_back();
	L.insert(L.end(),U.begin(),U.end());
	return L;
}

ld distancePointLine(point  a,  point  v,  point  p){
	//line: a + tv, point p
	return abs(v.cross(p - a)) / v.length();
}
ld getAns(vector<point>& P){
	ld ans = 1e9;
	int n = P.size();
	for(int i = 0, j = 1; i < n; i++){
		while((P[(i+1)%n]-P[i]).cross(P[(j+1)%n]-P[j]) > 0){
		       	j = (j+1)%n;
		}
		//cout << "i: " << i << " j: " << j << '\n';
		ans = min(ans, distancePointLine(P[i],P[(i+1)%n]-P[i],P[j]));
	}
	return ans;
}
int main(){
	//ios_base::sync_with_stdio(0);
	//cin.tie(0);
	//cout.tie(0);
	int cont = 0;
	while(cin >> n && n){
		vector<point> P(n);
		forn(i,n) cin >> P[i].x >> P[i].y;
		vector<point> convex = convexHull(P);
		ld ans = getAns(convex);
		ans *= 1000;
		ans = trunc(ans);
		ans /= 10;
		ans = ceil(ans);
		ans /= 100;
		cout << "Case " << ++cont << ": ";
		cout << fixed << setprecision(2) << ans << '\n';
	}
	return 0;
}
