/*
 * Judge: Codeforces
 * Topic: Binary search
 * Link: https://codeforces.com/problemset/problem/448/D
 */
#include<bits/stdc++.h>
using namespace std;
long long k;
long long n;
long long m, mitadf, res;
long long rf;
long long lf;


long long fun(long long num)
{
	long long resp = 0;
	for(int i = 1; i <= m; i++)
	{
		resp += min((num - 1)/i, n);
	}
	return resp;
}

int main()
{
	cin >> m >> n >> k;
	lf = 1;
	rf = m*n;
	while(lf <= rf)
	{
		mitadf = (lf + rf)/2;
		long long ant = fun(mitadf);
		if(ant >= k)
		{
			rf = mitadf - 1;
		}
		else
		{
			res = mitadf;
			lf = mitadf + 1;
		}
	}
	cout << res << "\n";
	return 0;
}
