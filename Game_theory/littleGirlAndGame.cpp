/*
 * Judge: Codeforces
 * Topic: Game theory
 * Link: https://codeforces.com/problemset/problem/276/B
 */
#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
ll let[26];
string s;
ll sum;
int imp;

int main()
{
	cin >> s;
	for(int i = 0; i < s.size(); i++)
		let[s[i]-'a']++;
	for(int i = 0; i < 26; i++){
		if(let[i] & 1) imp++;
		sum += let[i];
	}
	if(imp == 1 || imp == 0){
		cout << "First\n";
	}
	else if(sum & 1){
		cout << "First\n";
	}
	else
		cout << "Second\n";
	return 0;
}
