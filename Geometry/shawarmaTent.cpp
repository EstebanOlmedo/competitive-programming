/* 
 * Judge: Codeforces
 * Topic: Geometry, math, greedy
 * Link: https://codeforces.com/problemset/problem/1271/C
 */
#include<bits/stdc++.h>
#define se second
#define fi first
#define forn(i,n) for(int i=0;i<n;i++)
using namespace std;
const int N = 200005;

struct point{
	int x,y;
	point(): x(0), y(0) {}
	point(int _x, int _y): x(_x), y(_y) {}
};
point al[N];
point esc;
int n;
bool compX(const point& a, const point& b){
	if(a.x == b.x) return a.y < b.y;
	return a.x < b.x;
}
bool compY(const point& a, const point& b){
	if(a.y == b.y) return a.x < b.x;
	return a.y < b.y;
}
pair<int,point> maxHor(){
	int i = 0;
	int der=0,izq=0;
	sort(al,al+n,compX);
	while(i < n && al[i].x < esc.x){
		izq++;
		i++;
	}
	while(i < n && al[i].x == esc.x) i++;
	der = n - i;
	if(der > izq){
		return {der,point(esc.x+1,esc.y)};
	}
	else
		return {izq,point(esc.x-1,esc.y)};
}
pair<int,point> maxVer(){
	int i = 0;
	int arr=0,abj=0;
	sort(al,al+n,compY);
	while(i < n && al[i].y < esc.y){
		abj++;
		i++;
	}
	while(i < n && al[i].y == esc.y) i++;
	arr = n - i;
	if(arr > abj){
		return {arr,point(esc.x,esc.y+1)};
	}
	else
		return {abj,point(esc.x,esc.y-1)};

}
int main(){
	cin >> n >> esc.x >> esc.y;
	forn(i,n){
		cin >> al[i].x >> al[i].y;
	}
	pair<int,point> ver = maxVer();
	pair<int,point> hor = maxHor();
	if(ver.fi > hor.fi){
		cout <<ver.fi << '\n' << ver.se.x << ' ' << ver.se.y << '\n';
	}
	else
		cout <<hor.fi << '\n' << hor.se.x << ' ' << hor.se.y << '\n';
	return 0;
}
