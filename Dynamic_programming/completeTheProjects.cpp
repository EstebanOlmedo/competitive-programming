/*
 * Judge: Codeforces
 * Topic: Dynamic programming
 * Link: https://codeforces.com/problemset/problem/1203/F2 
 */
#include<bits/stdc++.h>
using namespace std;
typedef pair<int, int> pii;
typedef long long ll;
int n, r;
int a, b;
int res;
int dp[101][60001];
bool usd[101][60001];
bool func(pii a, pii b){
	return a.first == b.first ? a.second > b.second: a.first < b.first;
}
bool func2(pii a, pii b){
	return a.first + a.second > b.second + b.first;
}
void sumarPositivos(vector<pii>& positivos){
	sort(positivos.begin(), positivos.end(), func);
	int i = 0;
	while(i < positivos.size() && positivos[i].first <= r){
		r += positivos[i].second;
		i++;
		res++;
	}
}
int sumarNegativos(vector<pii>& negativos, int elem, int rating){
	if(elem >= negativos.size() || rating < 0) return 0;
	if(usd[elem][rating]) return dp[elem][rating];
	if(rating + negativos[elem].second >= 0 && rating >= negativos[elem].first){
		int a = 1 + sumarNegativos(negativos, elem + 1, rating + negativos[elem].second);
		int b = sumarNegativos(negativos, elem + 1, rating);
		dp[elem][rating] = max(a, b);
		usd[elem][rating] = true;
	}
	else
		dp[elem][rating] = sumarNegativos(negativos, elem + 1, rating);
	return dp[elem][rating];
}
int main()
{
	vector<pii> negativos;
	vector<pii> positivos;
	cin >> n >> r;
	while(n--){
		cin >> a >> b;
		if(b < 0)
			negativos.push_back(pii(a, b));
		else
			positivos.push_back(pii(a, b));
	}
	sumarPositivos(positivos);
	sort(negativos.begin(), negativos.end(), func2);
	//for(auto x : negativos){ cout << "F: " << x.first << ", S: " << x.second << "\n";}
	res += sumarNegativos(negativos, 0, r);
	cout << res << "\n";
}
