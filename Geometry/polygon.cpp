/*
 * Judge: UVA
 * Topic: Geometry, polygon
 * Link: https://onlinejudge.org/index.php?option=onlinejudge&Itemid=8&page=show_problem&problem=575
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
#define pb push_back
#define lli long long int
#define ld long double
using namespace std;
struct point{
	lli x,y;
	point(): x(0), y(0){}
	point(lli _x, lli _y): x(_x), y(_y){}

	lli cross(const point& p)const{return x*p.y-y*p.x;}
	lli dot(const point& p)const{return x*p.x+y*p.y;}
	point operator-(const point& p)const{return point(x-p.x, y-p.y);}
};
double orient(point a, point b, point c){
	return (b-a).cross(c-a);
}
bool inDisk(point a, point b, point p){
	return (a-p).dot(b-p) <= 0;
}
bool onSegment(point a, point b, point p){
	return orient(a,b,p) == 0 && inDisk(a,b,p);
}
bool above(point a, point p){
	return p.y >= a.y;
}
bool crossesRay(point a, point p, point q){
	return (above(a,q) - above(a,p)) * orient(a,p,q) > 0;
}
bool isInPolygon(vector<point>& p, point a, bool strict = true){
	int numCrosses = 0;
	int n = p.size();
	forn(i,n){
		if(onSegment(p[i], p[(i+1)%n],a))
			return !strict;
		numCrosses += crossesRay(a,p[i], p[(i+1)%n]);
	}
	//cout << "NUMCROSSES: " << numCrosses << '\n';
	return numCrosses & 1;
}
int main(){
	int n;
	while(cin >> n && n){
		vector<point> P(n);
		point a;
		forn(i,n) cin >> P[i].x >> P[i].y;
		cin >> a.x >> a.y;
		if(isInPolygon(P,a,false)) cout << "T\n";
		else cout << "F\n";
	}
	return 0;
}
