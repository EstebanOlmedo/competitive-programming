/*
 * Judge: EOlymp
 * Topic: Game theory
 * Link: https://www.e-olymp.com/en/problems/1011
 */
#include<bits/stdc++.h>
#define forn(i,n) for(int i = 0; i < n; i++)
using namespace std;
typedef long long ll;
int n;
ll num[1003];
ll numXor;

ll mostSignificantBit(ll n){
	int i = 0;
	int indMostSig = 0;
	while(n){
		if(n & 1) indMostSig = i;
		i++;
		n>>=1;
	}
	return 1ll << indMostSig;
}
ll obtenerMov(){
	ll res = 0;
	ll mostSig = mostSignificantBit(numXor);
	forn(i,n){ if(mostSig & num[i]) res++;}
	return res;
}

int main(){
	while(true){
		cin >> n;
		if(!n) break;
		numXor = 0;
		forn(i,n){
			cin >> num[i];
			numXor = numXor xor num[i];
		}
		if(!numXor) cout << "0\n";
		else cout << obtenerMov() << '\n';
	}
	return 0;
}
